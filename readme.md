# Deploying Instance Template and Instance Group




## Deployment

To deploy this project run

```bash
  gcloud init
  gcloud auth application-default login
  terraform init
  terraform plan
  terraform apply
```



## Screenshot - Instance Template

![IT Screenshot](screenshots/it.png)

## Screenshot - Instance Group

![IG Screenshot](screenshots/ig.png)

## Screenshot - VM instances

![VM Screenshot](screenshots/VM-instances.png)

## Screenshot - Health Check

![HC Screenshot](screenshots/hc.png)
