provider "google" {
    project = "${var.project}"
    region = "${var.region}"
}

resource "google_compute_instance_template" "instance-template" {
    name = "${var.name}"
    machine_type = "${var.machinetype}"
    disk {
      source_image = "${var.image}"
    }
    network_interface {
    network = "default"
  }
}

resource "google_compute_health_check" "autohealing" {
  name                = "autohealing-health-check"
  check_interval_sec  = 5
  timeout_sec         = 5
  healthy_threshold   = 2
  unhealthy_threshold = 10 # 50 seconds

  http_health_check {
    request_path = "/healthz"
    port         = "8080"
  }
}

resource "google_compute_instance_group_manager" "grpmng" {
    name = "${var.name}-mig"
    base_instance_name = google_compute_instance_template.instance-template.name
    zone = "${var.zone}"

    version {
    instance_template  = google_compute_instance_template.instance-template.id
  }

  target_size  = 2



    auto_healing_policies {
    health_check      = google_compute_health_check.autohealing.id
    initial_delay_sec = 300
  }
  
}

